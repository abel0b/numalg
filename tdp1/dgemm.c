#include "my_cblas.h"
#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#define TILE 2
#define MIN(a,b) (((a)<(b))?(a):(b))

void my_dgemm_scalaire(const int m, const int n, const double * A, const int lda, const double * B, const int ldb, double *C, const int ldc) {
	int i, j;
	for(i=0; i<n; i++) {
		for(j=0;j<m;j++) {
			C[j*ldc+i] = my_ddot(m, &A[i*lda], 1, &B[j*ldb], 1);
		}
	}
}


void my_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double *A, const int lda, const double *B, const int ldb, const double beta, double *C, const int ldc) {
	assert(Order == 0);
	assert(TransA == 0);
	assert(TransB == 0);
	assert(K == 0);
	int i, j, k, ii, jj;
	for(i=0; i < N; i=i+TILE)
		for(j=0; j < M; j=j+TILE)
				for(ii=i; ii < MIN(i+TILE,N); ii++)
					for(jj=j; jj < MIN(j+TILE,M); jj++)
						for(k=0; k < M; k++)
							C[ii+jj*ldc] += A[ii+k*lda]*B[k+jj*ldb];
}

int my_dgetf2(const enum CBLAS_ORDER Order, int m, int n, double* a, int lda, int* ipiv ) {
	assert(ipiv == NULL);
	int i,j,k;
	for(k=0;k<n;k++) {
		for(i=k+1; i<n; i++) {
			a[i+k*lda] /= a[k+k*lda];
			for(j=k+1;j<n;j++) {
				a[i+j*lda] -= a[i+k*lda]*a[k+j*lda];
			}
		}
	}
}

int my_dgetrf(const enum CBLAS_ORDER Order, int m, int n, double* a, int lda, int* ipiv ) {
	assert(ipiv == NULL);
	int i,j,k;
	for(k=0;k<n;k++) {
		for(i=k+1; i<n; i++) {
			a[i+k*lda] /= a[k+k*lda];
			for(j=k+1;j<n;j++) {
				a[i+j*lda] -= a[i+k*lda]*a[k+j*lda];
			}
		}
	}
}

int my_dtrsm(char side, char uplo, char transa, char *	diag, int m, int n, double alpha, double * a, int lda, double * b, int ldb) {
	assert(side == 'l');
	assert(uplo == 'u');
	int i, j, k;
	double lambda;

	for(j=0; j<n; j++) {
		for(i=j+1; i<n; i++) {
			lambda = a[i+j*lda]/a[j+j*lda];
			for(k=j; k<n; k++) {
				a[i+k*lda] -= lambda * a[j+k*lda];
			}
			b[i] -= lambda*b[j];
		}
		b[j] /= a[j+j*lda];
	}
}
