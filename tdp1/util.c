#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void affiche(int m, int n, double * a, int lda, FILE * flux) {
	int i, j;
	for(i=0; i<n; i++) {
		for(j=0; j<m; j++) {
			fprintf(flux, "%f ", a[j*lda+i]);
		}
		fprintf(flux, "\n");
	}
}


double * init_vec(int n) {
	double * vec = malloc(sizeof(double) * n);
	bzero(vec, sizeof(double)*n);
	return vec;
}

double * init_mat(int m, int n) {
	double * mat = malloc(sizeof(double)*m*n);
	bzero(mat, sizeof(double)*m*n);
	return mat;
}
