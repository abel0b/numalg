#include "my_cblas.h"

double my_ddot(const int N, const double *X, const int incX, const double *Y, const int incY) {
	double result = 0.0;
	int i;
	for (i=0; i<N; i++) {
		result += X[i*incX] * Y[i*incY];
	}

	return result;
}

