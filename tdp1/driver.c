#include <stdlib.h>
#include <stdio.h>
#include "util.h"
#include "my_cblas.h"
#include <time.h>
#include <sys/time.h>

#define EXPE_ITER 10

long get_microtime(){
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int main() {
	int i;
	double tmp;
	long t0, t1;
	double * mat1, * mat2, * mat3;

	mat1 = init_mat(2, 2);
	mat1[0] = 1.0;
	mat1[1] = 0.0;
	mat1[2] = 0.0;
	mat1[3] = 1.0;

	affiche(2, 2, mat1, 2, stdout);

	mat2 = init_mat(3, 2);
    mat2[0] = 0.0;
    mat2[1] = 1.0;
    mat2[2] = 0.0;
    mat2[3] = 0.0;
	mat2[4] = 0.0;
    mat2[5] = 1.0;

    affiche(2, 2, mat2, 3, stdout);

	free(mat1);
    free(mat2);

	double * vec1 = init_vec(2);
	vec1[0] = 2.0;
	vec1[1] = 4.0;

	double * vec2 = init_vec(2);
	vec2[0] = 8.0;
	vec2[1] = 16.0;

	double dot = my_ddot(2, vec1, 1, vec2, 1);

	printf("dot=%f\n", dot);

	free(vec1);
    free(vec2);

	int m;

	double * v1 = malloc(sizeof(double) * 1000000);
	double * v2 = malloc(sizeof(double) * 1000000);
	for(m=50; m <1000000; m += 0.25 * m) {
		t0 = get_microtime();
		for (i=0; i<EXPE_ITER;i++) {
			tmp = my_ddot(m, v1, 1, v2, 1);
		}
		t1 = get_microtime();
		double flops = (double)m / ((t1-t0)/1000000.0);
		printf("%d %f\n", m, flops);
	}
	free(v1);
	free(v2);


	mat1 = init_mat(2, 2);
    mat1[0] = 1.0;
    mat1[1] = 0.0;
    mat1[2] = 0.0;
    mat1[3] = 1.0;

	mat2 = init_mat(2, 2);
    mat2[0] = 2.0;
    mat2[1] = 0.0;
    mat2[2] = 3.0;
    mat2[3] = 4.0;

	mat3 = init_mat(2, 2);

	my_dgemm_scalaire(2, 2, mat1, 2, mat2, 2, mat3, 2);

	affiche(2, 2, mat1, 2, stdout);
	affiche(2, 2, mat2, 2, stdout);
	affiche(2, 2, mat3, 2, stdout);

	free(mat3);
	mat3 = init_mat(2, 2);
	my_dgemm(0, 0, 0, 2, 2, 0, 1.0, mat1, 2, mat2, 2, 1.0, mat3, 2);
	affiche(2, 2, mat3, 2, stdout);

	free(mat1);
	free(mat2);
	free(mat3);

	mat1 = init_mat(3, 3);
	mat1[0] = 2;
	mat1[1] = -1;
	mat1[2] = 0;
	mat1[3] = -1;
	mat1[4] = 2;
	mat1[5] = -1;
	mat1[6] = 0;
	mat1[7] = -1;
	mat1[8] = 2;
	affiche(3, 3, mat1, 3, stdout);
	my_dgetf2(0, 3, 3, mat1, 3, NULL);
	affiche(3, 3, mat1, 3, stdout);
	free(mat1);

	printf("=== dtrsm\n");
	mat1 = init_mat(3, 3);
	mat2 = init_mat(3, 3);
	mat1[0] = 1;
	mat1[1] = 0;
	mat1[2] = 0;
	mat1[3] = 0;
	mat1[4] = 1;
	mat1[5] = 0;
	mat1[6] = 0;
	mat1[7] = 0;
	mat1[8] = 1;
	vec1 = init_vec(3);
	vec2 = init_vec(3);
	vec1[0] = 2;
	vec1[1] = -1;
	vec1[2] = 8;
	affiche(1, 3, vec1, 3, stdout);
	my_dtrsm('l', 'u', '\0', '\0', 3, 3, 2, mat1, 3, vec1, 3);
	my_dgemm(0, 0, 0, 3, 3, 0, 1.0, mat1, 3, vec1, 3, 1.0, mat2, 3);

	affiche(3, 3, mat1, 3, stdout);
	affiche(1, 3, vec1, 3, stdout);
	affiche(1, 3, vec2, 3, stdout);

	free(mat1);
	free(vec1);
	free(vec2);

	return EXIT_SUCCESS;
}
