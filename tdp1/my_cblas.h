#ifndef MY_CBLAS_H
#define MY_CBLAS_H

enum CBLAS_TRANSPOSE {
   CblasNoTrans=111,
   CblasTrans=112,
   CblasConjTrans=113,
   AtlasConj=114,
};
enum CBLAS_ORDER {
    CblasRowMajor=101,
    CblasColMajor=102,
};

double my_ddot(const int N, const double *X, const int incX, const double *Y, const int incY);

void my_dgemm_scalaire(const int m, const int n, const double * A, const int lda, const double * B, const int ldb, double * C, const int ldc);

void my_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double *A, const int lda, const double *B, const int ldb, const double beta, double *C, const int ldc);

int my_dgetrf(const enum CBLAS_ORDER Order, int m, int n, double* a, int lda, int* ipiv );

int my_dgetf2(const enum CBLAS_ORDER Order, int m, int n, double* a, int lda, int* ipiv );

int my_dtrsm(char side, char uplo, char transa, char *	diag, int m, int n, double alpha, double *	a, int lda, double * b, int	ldb);

#endif
