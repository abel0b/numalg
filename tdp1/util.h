#ifndef UTIL_H
#define UTIL_H

void affiche(int m, int n, double * a, int lda, FILE * flux);

double * init_vec(int n);

double * init_mat(int m, int n);


#endif
